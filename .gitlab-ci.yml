---
image: docker:latest

stages:
  - test
  - build
  - docs
  - deploy

variables:
  CVMFS_BASE_DIR: "/cvmfs/ligo-containers.opensciencegrid.org/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}"
  DOCKER_DRIVER: overlay
  TAG: $CI_REGISTRY_IMAGE:latest
  NIGHTLY: $CI_REGISTRY_IMAGE:nightly
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

before_script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY

test-macos:
  tags:
    - macos_highsierra
  stage: test
  variables:
    INSTALL_PATH: "${CI_PROJECT_DIR}/miniconda"
  only:
    - master
    - merge_requests
    - schedules
  before_script: []
  script:
    - curl -o ./miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
    - /bin/bash miniconda.sh -b -p ${INSTALL_PATH}
    - source ${INSTALL_PATH}/etc/profile.d/conda.sh
    - conda update --yes conda
    - conda env create -f environment-py27.yml
    - conda env create -f environment-py36.yml
    - conda env create -f environment-py37.yml

test-linux:
  stage: test
  only:
    - merge_requests
  script:
    - docker build --pull --tag ${CI_REGISTRY_IMAGE}:test --build-arg INSTALL_PATH=/opt/conda .

build-latest:
  stage: build
  only:
    - master
  script:
    - docker build --no-cache --pull -t $TAG --build-arg INSTALL_PATH=${CVMFS_BASE_DIR}/latest .
    - docker push $TAG

build-nightly:
  stage: build
  only:
    - schedules
  script:
    - docker build --no-cache --pull -t $NIGHTLY -f Dockerfile-singularity .
    - docker push $NIGHTLY

# -- documentation

docs:
  stage: docs
  dependencies:
    - build-latest
  only:
    - schedules
  before_script:
    # install python
    - apk add python3
    - export LSCSOFT_CONDA_DOCKER_IMAGE="${NIGHTLY}"
    - python3 -m pip install -r docs/requirements.txt
  script:
    - cd docs
    - python3 -m sphinx -M html . ../build/sphinx -vv
    # remove index.html in links because duncan is _really_ pedantic
    - find ../build/sphinx/html -name "*.html" -type f -exec sed -i "s/index.html//g" {} +
  artifacts:
    paths:
      - build/sphinx/html
  cache:
    paths:
      - .cache/pip

pages:
  stage: deploy
  dependencies:
    - docs
  only:
    - schedules
  script:
    - mv build/sphinx/html public
  artifacts:
    paths:
      - public
