.. _lscsoft-conda-usage:

===================
Using LSCSoft Conda
===================

.. note::

   LSCSoft Conda is fully supported on 64-bit Linux and macOS, support for
   other platforms is on a best-effort basis only.

.. note::

   Linux users, especially those using the LIGO Data Grid computing centres,
   may find it easier to just use one of the
   :ref:`lscsoft-conda-pre-built-environments`.

----------------
1. Install conda
----------------

Using software provided by LSCSoft Conda requires the
`conda <https://conda.io>`_ package manager.
We recommend `installing Miniconda
<https://conda.io/projects/conda/en/latest/user-guide/install/index.html>`_,
however you can also `install conda using your Linux package manager
<https://www.anaconda.com/rpm-and-debian-repositories-for-miniconda/>`_.

-------------------------------
2. Add the conda-forge channnel
-------------------------------

All LSCSoft Conda packages are distributed on the conda-forge channnel.

.. code-block:: shell

   conda config --add channels conda-forge

------------------------
3. Create an environment
------------------------

You should now be able to create a conda environment:

.. code-block:: bash

   conda create --name lscsoft python=3.7 gwpy pycbc

